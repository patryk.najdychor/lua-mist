local Mist = {}
Mist.__index = Mist

local _loadedClasses = {}
local postponedLoad = {}
local _postponedInterfaces = {}

local function splitClassPath(classPath)
  local splittedClassPath = {}
  classPath = classPath .. "."
  for part in classPath:gmatch("(.-)%.") do
    table.insert(splittedClassPath, part)
  end

  local className = splittedClassPath[#splittedClassPath]
  table.remove(splittedClassPath, #splittedClassPath)

  return splittedClassPath, className
end

local function getClassFromPath(classPath)
  local splittedClassPath, className = splitClassPath(classPath)

  local tab = _G
  if #splittedClassPath > 0 then
    for i = 1, #splittedClassPath, 1 do
      if tab[splittedClassPath[i]] then
        tab = tab[splittedClassPath[i]]
      else
        return false
      end
    end
  end

  if tab[className] then
    return tab[className]
  else
    return false
  end
end

local function insertIntoPackage(path, classOrInterface)
  local splittedPath, className = splitClassPath(path)
  local tab = _G
  for i = 1, #splittedPath, 1 do
    if not tab[splittedPath[i]] then
      tab[splittedPath[i]] = {}
    end
    tab = tab[splittedPath[i]]
  end
  classOrInterface._path = path
  tab[className] = classOrInterface
end

function Mist.constructor()
end

function Mist:new()
end

function Mist:abstract(a, b, c, d)
  local abstract = Mist:class(a, b, c, d)
  abstract.__abstract = true
  return abstract
end

function Mist:class(a, b, c, d)
  --name
  local classPath = a
  --extends
  local parentClassPath
  --implements
  local interfacePaths
  --function
  local classBuilder

  if a and b and c and d then
    parentClassPath = b
    interfacePaths = c
    classBuilder = d
  elseif a and b and c then
    classBuilder = c
    if type(b) == "table" then
      interfacePaths = b
    else
      parentClassPath = b
    end
  elseif a and b then
    classPath = a
    classBuilder = b
  end

  local parentClass
  if parentClassPath then
    parentClass = getClassFromPath(parentClassPath)
    if parentClass and parentClass:isInterface() then
      error("Cannot extend an interface")
    end
  else
    parentClass = Mist
  end

  local interfaces = {}
  local interfaceNotLoaded = false
  --load interfaces
  if interfacePaths and #interfacePaths > 0 then
    for i, interfacePath in ipairs(interfacePaths) do
      local interface = getClassFromPath(interfacePath)
      if interface then
        table.insert(interfaces, interface)
      else
        interfaceNotLoaded = true
      end
    end
  end

  if not parentClass or interfaceNotLoaded then
    --skip saving if this class is already in postponedLoad table
    for k, classDetails in pairs(postponedLoad) do
      if classDetails.a == classPath then
        return
      end
    end

    local classDetails = {
      a = a,
      b = b,
      c = c,
      d = d
    }
    table.insert(postponedLoad, classDetails)
    --postpone load because we need parent or interface to load first
    return
  end

  if parentClass:isInterface() then
    error("Attempted to extend an interface", 2)
  end

  --create a new class
  local cls = {}
  for k, v in pairs(parentClass) do
    if k:find("__") == 1 then
      cls[k] = v
    end
  end
  cls.__index = cls
  cls.superClass = parentClass
  setmetatable(cls, parentClass)

  local mergedInterfaces = {}
  if cls.__interfaces then
    for i, interface in ipairs(cls.__interfaces) do
      table.insert(mergedInterfaces, interface)
    end
  end
  for i, interface in ipairs(interfaces) do
    table.insert(mergedInterfaces, interface)
  end
  cls.__interfaces = mergedInterfaces

  local classMethods = classBuilder()
  for k, v in pairs(classMethods) do
    cls[k] = v
  end

  --verify that static methods are implemented
  for i, interface in ipairs(interfaces) do
    if interface["static"] then
      for k, v in pairs(interface["static"]) do
        if not cls[k] then
          error("Static function not implemented in class: " .. classPath .. ", function name: " .. k .. ", from interface: " .. interface:getPath())
        end
      end
    end
  end

  cls.__interface = false
  cls.__abstract = false
  insertIntoPackage(classPath, cls)
  local existsInLoadedClasses = false
  for i, class in ipairs(_loadedClasses) do
    if class:getPath() == cls:getPath() then
      existsInLoadedClasses = true
    end
  end
  if not existsInLoadedClasses then
    table.insert(_loadedClasses, cls)
  end

  --check for not loaded child classes
  for _, classDetails in pairs(postponedLoad) do
    if classDetails.b == classPath then
      Mist:class(classDetails.a, classDetails.b, classDetails.c, classDetails.d)
    end
  end
end

function Mist:getClasses(someType)
  local classes = {}
  for i, class in ipairs(_loadedClasses) do
    if class:is(someType) then
      table.insert(classes, class)
    end
  end
  return classes
end

function Mist:getClassName()
    local path = self:getPath()
    local _, className = splitClassPath(path)
    return className
end

function Mist:getPath()
  return self._path
end

function Mist:getType()
  if self.__abstract then
    return "abstract"
  end

  if self.__interface then
    return "interface"
  end

  return "class"
end

function Mist:interface(interfacePath, b, c)
  local parentInterfacePaths
  local interfaceBuilder

  local parentInterfaces = {}

  if type(b) == "function" then
    interfaceBuilder = b
  else
    parentInterfacePaths = b
    interfaceBuilder = c
    for _, parentInterfacePath in ipairs(parentInterfacePaths) do
      local parentInterface = getClassFromPath(parentInterfacePath)
      --one of parent interfaces did not load yet
      if not parentInterface then
        for k, interface in pairs(_postponedInterfaces) do
          --this interface already exists in _postponedInterfaces
          if interface[1] == interfacePath then
              return
          end
        end
        table.insert(_postponedInterfaces, {interfacePath, b, c})
        return
      end
      table.insert(parentInterfaces, parentInterface)
    end
  end

  local interface = {}
  interface.__index = interface
  interface.__interface = true
  interface.__extends = parentInterfaces
  setmetatable(interface, Mist)

  local interfaceFunctionNames = interfaceBuilder()
  if not interfaceFunctionNames["static"] then interfaceFunctionNames["static"] = {} end
  for k, parentInterface in pairs(parentInterfaces) do
    for k2, v2 in pairs(parentInterface) do
      local isFunction = (type(v2) == "function" and string.find(k, "_") ~= 1)
      local isStatic = (k2 == "static" and type(v2) == "table")
      if isFunction then
        interfaceFunctionNames[k2] = v2
      elseif isStatic then
        for k3, v3 in pairs(v2) do
          interfaceFunctionNames["static"][k3] = v3
        end
      end
    end
  end

  interface["static"] = {}

  for k, v in pairs(interfaceFunctionNames) do
    if type(v) == "string" then
      interface[v] = function() end
    elseif type(v) == "function" then
      interface[k] = function() end
    elseif k == "static" and type(v) == "table" then
      for k2, v2 in pairs(v) do
        if type(v2) == "function" then
          interface["static"][k2] = function() end
        elseif type(v2) == "string" then
          interface["static"][v2] = function() end
        end
      end
    end
  end

  insertIntoPackage(interfacePath, interface)

  --avoid infinite loops
  local postponedInterfacesCopy = {}
  if unpack then
    postponedInterfacesCopy = {unpack(_postponedInterfaces)}
  elseif table.unpack then
    postponedInterfacesCopy = {table.unpack(_postponedInterfaces)}
  end
  --try to load postponed interfaces
  for k, interfaceDetails in pairs(postponedInterfacesCopy) do
    parentInterfaces = interfaceDetails[2]
    if type(parentInterfaces) == "table" then
      local foundSelf = false
      for _, parentInterfacePath in pairs(parentInterfaces) do
        if parentInterfacePath == interfacePath then
          foundSelf = true
        end
      end
      if foundSelf then
        Mist:interface(interfaceDetails[1], interfaceDetails[2], interfaceDetails[3])
      end
    end
  end

  --avoid infinite loops
  local postponedLoadCopy = {}
  if unpack then
    postponedLoadCopy = {unpack(postponedLoad)}
  elseif table.unpack then
    postponedLoadCopy = {table.unpack(postponedLoad)}
  end
  --try to load postponed implementing classes
  for _, classDetails in pairs(postponedLoadCopy) do
    Mist:class(classDetails.a, classDetails.b, classDetails.c, classDetails.d)
  end
end

function Mist:is(T)
  if self == T then
    return true
  end
  local mt = getmetatable(self)
  while mt do
    if mt == T then
      return true
    end
    mt = getmetatable(mt)
  end
  local interfaces = self.__interfaces
  if interfaces then
    for i, interface in ipairs(interfaces) do
      if interface == T then
        return true
      end
      for i2, extendedInterface in ipairs(interface.__extends) do
        if extendedInterface == T then
          return true
        end
      end
    end
  end
  local isInterface = self:isInterface()
  if isInterface and self.__extends then
    for i, v in ipairs(self.__extends) do
      if v == T then
        return true
      end
    end
  end

  return false
end

function Mist:isAbstract()
  if self:getType() == "abstract" then
    return true
  end
  return false
end

function Mist:isClass()
  if self:getType() == "class" then
    return true
  end
  return false
end

function Mist:isInterface()
  if self:getType() == "interface" then
    return true
  end
  return false
end

function Mist:isObject()
  if self:getType() == "object" then
    return true
  end
  return false
end

function Mist:loaded()
  for i, class in ipairs(_loadedClasses) do
    if class.staticConstructor then
      class.staticConstructor()
    end
  end
end

function Mist:__call(...)
  if self:isAbstract() then
    error("Tried to instantiate an abstract class", 2)
  end

  if self:isInterface() then
    error("Tried to instantiate an interface", 2)
  end

  local obj = setmetatable({}, self)
  local protected = setmetatable({}, {})

  local superClass = obj.superClass
  local inheritanceChain = {}

  --gathers all superclasses in the inheritance chain
  while superClass do
    table.insert(inheritanceChain, 1, superClass)
    superClass = inheritanceChain[1].superClass
  end
  table.insert(inheritanceChain, obj)

  local super
  for i, class in ipairs(inheritanceChain) do
      class.new(obj, protected, super)
      if i == #inheritanceChain then break end

      if not super then
        super = {}
      else
        super = {["super"] = super}
      end
      for key, value in pairs(obj) do
        if type(value) == "function" then
          super[key] = value
        end
      end
  end

  if obj.constructor then
    obj.constructor(obj, ...)
  end

  if #self.__interfaces > 0 then
    for i, interface in ipairs(self.__interfaces) do
      for k, v in pairs(interface) do
        local isFunction = (type(v) == "function")
        local notImplemented = not obj[k]
        local notDefaultObjectClassFunction = Mist[k] ~= v
        if isFunction and notImplemented and notDefaultObjectClassFunction then
          error("Class " .. self:getPath() .. ", Interface function not implemented: " .. k, 2)
        end
      end
    end
  end

  obj.getType = function()
    return "object"
  end

  return obj
end

return Mist
# Lua Mist
An object oriented framework for Lua.

## Features
 - classes
 - interfaces
 - abstract classes
 - private variables
 - protected variables
 - public variables
 - single inheritance
 - multiple interface implementation
 - asynchronous loading
 - easy package creation

## How to use
Drop Mist.lua file from this repository somewhere and do:

        local Mist = require("Mist")

## Tutorial
Check out examples before I make a proper one.

### Credits
Based on Classic by rxi
https://github.com/rxi/classic

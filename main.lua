pilcrow = { base = { Mist = require("Mist") } }

require("examples/MovableEntity")
require("examples/FastMovableEntity")
require("examples/Entity")

--asynchronous loading (interface loaded after implementing class)
require("examples/interface/ConcreteController")
require("examples/interface/IController")

local tests = {}

tests[1] = {}

tests[1].createBasicEntity = function()
    local entity = pilcrow.Entity()

    local type = entity:getType()

    return type == "object"
end

tests[2] = {}

tests[2].createMovableEntity = function()
    local movableEntity = pilcrow.MovableEntity()

    local type = movableEntity:getType()

    return type == "object"
end

tests[2].isMovableEntityAnEntity = function()
    local movableEntity = pilcrow.MovableEntity()

    local movableEntityIsEntity = movableEntity:is(pilcrow.Entity)

    return movableEntityIsEntity
end

tests[2].didMovableEntityInheritEntityMethods = function()
    local entity = pilcrow.Entity(5, 5)
    local movableEntity = pilcrow.MovableEntity(5, 5)

    local entityCoordinates = entity:getCoordinates()
    local movableEntityCoordinates = movableEntity:getCoordinates()

    return entityCoordinates == movableEntityCoordinates
end


tests[2].didMovableEntityOverrideEntityMethods = function()
    local entity = pilcrow.Entity(1, 1)
    local movableEntity = pilcrow.MovableEntity(1, 1)

    local whoIsEntity = entity:whoAmI()
    local whoIsMovableEntity = movableEntity:whoAmI()

    return not (whoIsEntity == whoIsMovableEntity)
end

tests[2].moveMovableEntity = function()
    local movableEntity = pilcrow.MovableEntity(5, 5)
    local originalX, originalY = movableEntity:getCoordinates()

    movableEntity:moveRight():moveRight()

    local newX, newY = movableEntity:getCoordinates()
    return originalX == (newX - 2)
end

tests[3] = {}

tests[3].createFastMovableEntity = function()
    local fastMovableEntity = pilcrow.FastMovableEntity()

    local fastMovableEntityType = fastMovableEntity:getType()

    return fastMovableEntityType == "object"
end

tests[3].isFastMovableEntityAMovableEntity = function()
    local fastMovableEntity = pilcrow.FastMovableEntity()

    local isMovableEntity = fastMovableEntity:is(pilcrow.MovableEntity)

    return isMovableEntity
end

tests[3].isFastMovableEntityAnEntity = function()
    local fastMovableEntity = pilcrow.FastMovableEntity()

    local isEntity = fastMovableEntity:is(pilcrow.Entity)

    return isEntity
end

tests[3].didFastMovableEntityMoveRightBy100 = function()
    local fastMovableEntity = pilcrow.FastMovableEntity(10, 10)
    local x, y = fastMovableEntity:getCoordinates()
    fastMovableEntity:moveRight()

    local newX, newY = fastMovableEntity:getCoordinates()
    return newX - x == 100
end

tests[4] = {}

tests[4].isIControllerAnInterface = function()
    local IController = pilcrow.test.gui.IController

    local iControllerType = IController:getType()

    return iControllerType == "interface"
end

tests[4].areIControllerFunctionsLoaded = function()
    local IController = pilcrow.test.gui.IController

    local func1 = IController.implementMe
    local func2 = IController.implementMeToo

    return (func1 and func2)
end

tests[4].isConcreteControllerImplementingIController = function()
    local ConcreteController = pilcrow.test.gui.ConcreteController
    local IController = pilcrow.test.gui.IController

    local isImplemeted = ConcreteController:is(IController)

    return isImplemeted
end

tests[4].isConcreteControllerCorrectlyOverridingIController = function()
    local concreteController = pilcrow.test.gui.ConcreteController()
    local IController = pilcrow.test.gui.IController

    local implementMeResult = concreteController:implementMe()
    local implementMeTooResult = concreteController:implementMeToo()
    local implementMeInterfaceResult = IController:implementMe()
    local implementMeTooInterfaceResult = IController:implementMeToo()

    local areFunc1ResultsDifferent = not (implementMeResult == implementMeInterfaceResult)
    local areFunc2ResultsDifferent = not (implementMeTooResult == implementMeTooInterfaceResult)

    return areFunc1ResultsDifferent and areFunc2ResultsDifferent
end

for i, tab in ipairs(tests) do
    for functionName, fun in pairs(tab) do
        print(i .. " " .. functionName)
        local result = fun()
        if result then
            print("     Success ")
        else
            print("     FAILED")
        end
    end
end
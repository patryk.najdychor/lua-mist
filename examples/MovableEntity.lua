pilcrow.base.Mist:class("pilcrow.MovableEntity", "pilcrow.Entity", function()
    return {
        new = function(MovableEntity, protected, super)
            function MovableEntity:constructor(x, y)
                super:constructor(x, y)
            end

            function MovableEntity:moveLeft()
                protected.x = protected.x - 1
                return self
            end

            function MovableEntity:moveRight()
                protected.x = protected.x + 1
                return self
            end

            function MovableEntity:whoAmI()
                return "I'm a MovableEntity"
            end
        end
    }
end)
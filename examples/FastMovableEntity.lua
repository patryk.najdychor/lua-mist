pilcrow.base.Mist:class("pilcrow.FastMovableEntity", "pilcrow.MovableEntity", function()
    return {
        new = function(FastMovableEntity, protected, super)
            function FastMovableEntity:constructor(x, y)
                super:constructor(x, y)
            end

            function FastMovableEntity:moveLeft()
                protected.x = protected.x - 100
                return self
            end

            function FastMovableEntity:moveRight()
                protected.x = protected.x + 100
                return self
            end

            function FastMovableEntity:whoAmI()
                return "I'm a FastMovableEntity"
            end
        end
    }
end)
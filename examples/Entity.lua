pilcrow.base.Mist:class("pilcrow.Entity", function()
    return {
        new = function(Entity, protected)
            function Entity:constructor(x, y)
                protected.x = x
                protected.y = y
            end

            function Entity:getCoordinates()
                return protected.x, protected.y
            end

            function Entity:whoAmI()
                return "I'm an Entity"
            end
        end
    }
end)
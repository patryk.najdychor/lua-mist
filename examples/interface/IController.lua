pilcrow.base.Mist:interface("pilcrow.test.gui.IController", function()
    return {
        --interface functions can be declared as strings
        "implementMe",
        "implementMeToo",
        --or empty functions
        justARegularFunction = function() end
    }
end)
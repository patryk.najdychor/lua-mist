pilcrow.base.Mist:class("pilcrow.test.gui.ConcreteController", {"pilcrow.test.gui.IController"}, function()
    return {
        new = function(ConcreteController, protected)
            function ConcreteController:implementMe()
                return "implemented"
            end

            function ConcreteController:implementMeToo()
                return "implemented too"
            end

            function ConcreteController:justARegularFunction()
            end
        end
    }
end)